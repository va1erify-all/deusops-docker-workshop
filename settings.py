import env
import environ

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': env('MYSQL_DATABASE', default='tracker'),
        'USER': env('MYSQL_USER', default='root'),
        'PASSWORD': env('MYSQL_PASSWORD', default='root'),
        'HOST': env('MYSQL_HOST', default='localhost'),
        'PORT': env('MYSQL_PORT', default='3306'),
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0",
        },
        'TEST_CHARSET': "uft8mb4",
        'TEST_COLLATION': "uft8mb4_unicode_ci"
    }
}
