import os.path

BASE_DIR = ""

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.path.join(BASE_DIR, 'db/db.sqlite3'),
    }
}

